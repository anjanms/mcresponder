#!/usr/bin/env python
# -*- coding: utf-8 -*-
#Protocol info found at http://www.wiki.vg/Protocol
#VarInt functions from https://gist.github.com/barneygale/1209061

import os, sys, signal, argparse, socket, struct, json

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def signal_handler(signal, frame):
    print 'Exiting'
    sock.close()
    sys.exit(0)

def unpack_varint(s):
    d = 0
    for i in range(5):
        b = ord(s.recv(1))
        d |= (b & 0x7F) << 7*i
        if not b & 0x80:
            break
    return d

def pack_varint(d):
    o = ""
    while True:
        b = d & 0x7F
        d >>= 7
        o += struct.pack("B", b | (0x80 if d > 0 else 0))
        if d == 0:
            break
    return o

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    
    #Parse arguments
    parser = argparse.ArgumentParser(description='A Minecraft server ping responder')
    parser.add_argument("-p", action="store", dest="port", help="port", default=25565)
    parser.add_argument("-c", action="store", dest="config", help="config file", default="mcresponder.json")
    
    args = vars(parser.parse_args())
    
    configPath = os.path.dirname(os.path.realpath(__file__)) + os.sep + args["config"]
    
    #Load or create config
    if os.path.isfile(configPath):
        with open(configPath, 'r') as configFile:
            json_data = json.dumps(json.load(configFile))
    else:
        config = dict()
        config["version"] = dict()
        config["version"]["name"] = "1.8"
        config["version"]["protocol"] = 47
        config["players"] = dict()
        config["players"]["max"] = 0
        config["players"]["online"] = 0
        config["description"] = dict()
        config["description"]["text"] = "§7MCResponder by:§9 anjanms\n§8https://bitbucket.org/anjanms/mcresponder"
        config["favicon"] = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAKRGlDQ1BJQ0MgUHJvZmlsZQAASA2dlndUFNcXx9/MbC+0XZYiZem9twWkLr1IlSYKy+4CS1nWZRewN0QFIoqICFYkKGLAaCgSK6JYCAgW7AEJIkoMRhEVlczGHPX3Oyf5/U7eH3c+8333nnfn3vvOGQAoASECYQ6sAEC2UCKO9PdmxsUnMPG9AAZEgAM2AHC4uaLQKL9ogK5AXzYzF3WS8V8LAuD1LYBaAK5bBIQzmX/p/+9DkSsSSwCAwtEAOx4/l4tyIcpZ+RKRTJ9EmZ6SKWMYI2MxmiDKqjJO+8Tmf/p8Yk8Z87KFPNRHlrOIl82TcRfKG/OkfJSREJSL8gT8fJRvoKyfJc0WoPwGZXo2n5MLAIYi0yV8bjrK1ihTxNGRbJTnAkCgpH3FKV+xhF+A5gkAO0e0RCxIS5cwjbkmTBtnZxYzgJ+fxZdILMI53EyOmMdk52SLOMIlAHz6ZlkUUJLVlokW2dHG2dHRwtYSLf/n9Y+bn73+GWS9/eTxMuLPnkGMni/al9gvWk4tAKwptDZbvmgpOwFoWw+A6t0vmv4+AOQLAWjt++p7GLJ5SZdIRC5WVvn5+ZYCPtdSVtDP6386fPb8e/jqPEvZeZ9rx/Thp3KkWRKmrKjcnKwcqZiZK+Jw+UyL/x7ifx34VVpf5WEeyU/li/lC9KgYdMoEwjS03UKeQCLIETIFwr/r8L8M+yoHGX6aaxRodR8BPckSKPTRAfJrD8DQyABJ3IPuQJ/7FkKMAbKbF6s99mnuUUb3/7T/YeAy9BXOFaQxZTI7MprJlYrzZIzeCZnBAhKQB3SgBrSAHjAGFsAWOAFX4Al8QRAIA9EgHiwCXJAOsoEY5IPlYA0oAiVgC9gOqsFeUAcaQBM4BtrASXAOXARXwTVwE9wDQ2AUPAOT4DWYgSAID1EhGqQGaUMGkBlkC7Egd8gXCoEioXgoGUqDhJAUWg6tg0qgcqga2g81QN9DJ6Bz0GWoH7oDDUPj0O/QOxiBKTAd1oQNYSuYBXvBwXA0vBBOgxfDS+FCeDNcBdfCR+BW+Bx8Fb4JD8HP4CkEIGSEgeggFggLYSNhSAKSioiRlUgxUonUIk1IB9KNXEeGkAnkLQaHoWGYGAuMKyYAMx/DxSzGrMSUYqoxhzCtmC7MdcwwZhLzEUvFamDNsC7YQGwcNg2bjy3CVmLrsS3YC9ib2FHsaxwOx8AZ4ZxwAbh4XAZuGa4UtxvXjDuL68eN4KbweLwa3gzvhg/Dc/ASfBF+J/4I/gx+AD+Kf0MgE7QJtgQ/QgJBSFhLqCQcJpwmDBDGCDNEBaIB0YUYRuQRlxDLiHXEDmIfcZQ4Q1IkGZHcSNGkDNIaUhWpiXSBdJ/0kkwm65KdyRFkAXk1uYp8lHyJPEx+S1GimFLYlESKlLKZcpBylnKH8pJKpRpSPakJVAl1M7WBep76kPpGjiZnKRcox5NbJVcj1yo3IPdcnihvIO8lv0h+qXyl/HH5PvkJBaKCoQJbgaOwUqFG4YTCoMKUIk3RRjFMMVuxVPGw4mXFJ0p4JUMlXyWeUqHSAaXzSiM0hKZHY9O4tHW0OtoF2igdRzeiB9Iz6CX07+i99EllJWV75RjlAuUa5VPKQwyEYcgIZGQxyhjHGLcY71Q0VbxU+CqbVJpUBlSmVeeoeqryVYtVm1Vvqr5TY6r5qmWqbVVrU3ugjlE3VY9Qz1ffo35BfWIOfY7rHO6c4jnH5tzVgDVMNSI1lmkc0OjRmNLU0vTXFGnu1DyvOaHF0PLUytCq0DqtNa5N03bXFmhXaJ/RfspUZnoxs5hVzC7mpI6GToCOVGe/Tq/OjK6R7nzdtbrNug/0SHosvVS9Cr1OvUl9bf1Q/eX6jfp3DYgGLIN0gx0G3QbThkaGsYYbDNsMnxipGgUaLTVqNLpvTDX2MF5sXGt8wwRnwjLJNNltcs0UNnUwTTetMe0zg80czQRmu836zbHmzuZC81rzQQuKhZdFnkWjxbAlwzLEcq1lm+VzK32rBKutVt1WH60drLOs66zv2SjZBNmstemw+d3W1JZrW2N7w45q52e3yq7d7oW9mT3ffo/9bQeaQ6jDBodOhw+OTo5ixybHcSd9p2SnXU6DLDornFXKuuSMdfZ2XuV80vmti6OLxOWYy2+uFq6Zroddn8w1msufWzd3xE3XjeO2323Ineme7L7PfchDx4PjUevxyFPPk+dZ7znmZeKV4XXE67m3tbfYu8V7mu3CXsE+64P4+PsU+/T6KvnO9632fein65fm1+g36e/gv8z/bAA2IDhga8BgoGYgN7AhcDLIKWhFUFcwJTgquDr4UYhpiDikIxQODQrdFnp/nsE84by2MBAWGLYt7EG4Ufji8B8jcBHhETURjyNtIpdHdkfRopKiDke9jvaOLou+N994vnR+Z4x8TGJMQ8x0rE9seexQnFXcirir8erxgvj2BHxCTEJ9wtQC3wXbF4wmOiQWJd5aaLSwYOHlReqLshadSpJP4iQdT8YmxyYfTn7PCePUcqZSAlN2pUxy2dwd3Gc8T14Fb5zvxi/nj6W6pZanPklzS9uWNp7ukV6ZPiFgC6oFLzICMvZmTGeGZR7MnM2KzWrOJmQnZ58QKgkzhV05WjkFOf0iM1GRaGixy+LtiyfFweL6XCh3YW67hI7+TPVIjaXrpcN57nk1eW/yY/KPFygWCAt6lpgu2bRkbKnf0m+XYZZxl3Uu11m+ZvnwCq8V+1dCK1NWdq7SW1W4anS1/+pDa0hrMtf8tNZ6bfnaV+ti13UUahauLhxZ77++sUiuSFw0uMF1w96NmI2Cjb2b7Dbt3PSxmFd8pcS6pLLkfSm39Mo3Nt9UfTO7OXVzb5lj2Z4tuC3CLbe2emw9VK5YvrR8ZFvottYKZkVxxavtSdsvV9pX7t1B2iHdMVQVUtW+U3/nlp3vq9Orb9Z41zTv0ti1adf0bt7ugT2ee5r2au4t2ftun2Df7f3++1trDWsrD+AO5B14XBdT1/0t69uGevX6kvoPB4UHhw5FHupqcGpoOKxxuKwRbpQ2jh9JPHLtO5/v2pssmvY3M5pLjoKj0qNPv0/+/tax4GOdx1nHm34w+GFXC62luBVqXdI62ZbeNtQe395/IuhEZ4drR8uPlj8ePKlzsuaU8qmy06TThadnzyw9M3VWdHbiXNq5kc6kznvn487f6Iro6r0QfOHSRb+L57u9us9ccrt08rLL5RNXWFfarjpebe1x6Gn5yeGnll7H3tY+p772a87XOvrn9p8e8Bg4d93n+sUbgTeu3px3s//W/Fu3BxMHh27zbj+5k3Xnxd28uzP3Vt/H3i9+oPCg8qHGw9qfTX5uHnIcOjXsM9zzKOrRvRHuyLNfcn95P1r4mPq4ckx7rOGJ7ZOT437j154ueDr6TPRsZqLoV8Vfdz03fv7Db56/9UzGTY6+EL+Y/b30pdrLg6/sX3VOhU89fJ39ema6+I3am0NvWW+738W+G5vJf49/X/XB5EPHx+CP92ezZ2f/AAOY8/wRDtFgAAAACXBIWXMAAAsTAAALEwEAmpwYAAAhGElEQVR4AcWb2Y9k53ne37PUvnf1OsPhaLgosSnFUMZRNCaNDGHoKjAMihgSzoWB+IKKCCrMVZKrpPMfRKRAQ0KuggCxyFBCkNwERuKRwcWwNKZIariInH3t6e6qrv3Ucs7J7/mqi+ymaUvikpyZ6upTdc73fe/2vM/7fqcD+390nHn+TLD6wGpw+ezlRFM+nz4fNO/f+edf+YO7v3LvP/vqG2+98Faqzzf//HT45D/9R94L++f67PM8vM9zcI0twRvti/73v3lu6s43H8i2kvYj1Ur92ysbzQczmYyNR9OX+9Hg2T/9F3/9I66Z6Lonvncy027ck7zw2Auxzj+v43NTwOam+Tc3TgYHBc+tFx6dzaZPTmaTh7zAs1xQsEKhYJliYI18wzZqqy+VC8Xnrl658eLmYy98oIgjt34/3tzcdJ7zWSviM1cAC/Xtn5z1Nx8+O9NiNzdPh9eOzx5L0/RbqZc8FGTMkolvvu9PkzQJZ7OZ8ZodaWxk7v3CMdu6s2vvXP7FS7EX/8kDX2o+f3Acs9PJZ62Iz0wBcnUJvHBZnZe61x+P4/ipTC48lS/mbNAZ2ng6nuRz+YyPCwwGQ4SfWi6bs0wuk+ZyOb4dZ/0wtdDPmpf6r5ql323n136wGPd5xn3hwDya89Mcn1oBf1NwC6rR756ZTWf/Msz4p4r5vMXT1BJLp2mcBGmc+hOEnsUzQznmeZ5lslkLQh9ZEYVLWVQchEEmX8qhBM+SafKqN02feeaP/rdkd5hw5nlD4Wc+UPgnVcInVoAEf2Bl21u46Pd++kTm/etb35jGk6fD0D8V+r6F09BKmcJsFsf+MI78Ur5oxUzeuqOBbfV2bJpMLRtmLUSW0Whkg/HQgiCwMBNKMUkYBkk2kw1LKMhGiXlJ8EqtUnnm3/7Bwz/0vG86UFXWOL+9ki485NdVxK+tAAl+ENU3z5/J3npt+5FiofjtYiX/4BTrTjpTWyk24nw250UEPEa2rJexOIltMI0sSidG/FuQ+lbOFEyKGaKAC7euWJSMrch5JV9yiphw13QyTf1pGty1umr5bN52W52XL21ff/Zn2z/50fnNT5c1fmUFfBTV8b7sY7/7Dx7N54tPFsu5h8JsYFFvgiUjLD4LlioNr1GrWZLBhePEZqOJRdOxjbF6kiRWy1Vsvdp0IdAd9m1rb8e2uzuWzWVtrbFq65VlywShDSYj2+60bHfQTrN+Ni4AKECnjaLI0ln6EgM8919ff+2/2ffNeYTS55Fb58gaRN2vcPxSBQjVz9pZ/+zmHNUFQn8Rtx+PJpMnp7PxgwECplPfYtBsGk/x3owHmJnyu47JZGoDXH42m9gUxM/gCYp7uXqj3LA4ja01aFktX7P12orFfmK96YCg8K0Q5C0PGCaz2PYGXdvhun48TEu50gwvYSgUO0lQVPblMJt9bidofACWCg07Tdbw/u70+bcq4KPgJsH/Z/v646kfP1Uo5E/lChnba/WxbjzJhEQrgR8o7sPQSHk2jsbWHw6wdmx3L99tR1c3rNPt2ttX3rV8MW/lStmG0cgqubIdWVqzcr5gqNDuYO1be1vmZ1CBH2DlxPKWsVKYt2qlZpl81tpRx9qDTsL1M8/zs6Va3mbjxCbj2auBec/+XnH1+cf2CZTkOIMhFufOKgd+/A0FfDTNSJMX348eJX6fzmbDUwWsW0whL0Dc3qgb9GZD1hlYDovL6orlPi6NR9hqddXuWlm3WrlqObJBp9+z929eMlCP60mLKKhRrNsX1o5i4Z5d3roGEPYtG2StVCyREFKboBThRS6TtaNLG2BD0W61d2yrc8c8/qGEBNyJw2wmU64UDDe00XD0aj4sfueP7vv6iw8/vOn4iFMEmnjMO8wsDysA9mb7sfMEqJ6/cOWRNE2e9gL7HcxqfgR78zOzwAI/tsTvz0DuOGIZgBxInSF+p+OpxaOpeZPYasWqNZpL5vken0+sFw1sF+sJDN3EfN4ozl1fGWCrs2vD8UAkyeAKViqVnacIM3ZbLdLmzHA1i8YRgs8MRunwJBfkEHySTOJpUsgVwiNrq7ZGOOX87CvDKPrOxf/+X370/X2MYKnk2w/x4UMFpE6h6ffSJzJv/ODyo6ztqWw+fBD2Zt7Is5JfiH2iN0Hw1qBjHSwli0twn4uJfSvnilYrVCzrZ6zT6Thg8/KBkcrMh9gORkPbQwHCgDwuXygVrMr1zQKu7Ydwg9jafe7rbbtQKpVKVuQlwrS7vW1QApO3KVTEIbLQyqVS3ZbKNevhTXd6LburuZ7UyxWRjiDMOg5h0XD68tibffcbj6y8+Nve9wWWktsVX04BGFdrSh/5d7/99bV7qv+htJQ7FRCD/fbIIgKLxQUb1VWvUak68Nrea9m11k3cOseCfGf11dKS1UtVLJKC3EMbxXPEn0wnorqW8jmzOC0PAUVN3CjXbaXWtKVK3QnU7hHbo64NZzBELFwqlK3eqFsE4u9s75BhhngPBuHmSl5ZZJmUWbCEkJJRusOerVWasMjQbvd30hiNVrKlcGmNdVF7tLb6r77xfy7/+x//p7f/bCEzt7rxeDMbD6Z/2G9Hp4a9SVppFqZwkEwhzIVN0LpZW3Lsbau1Y63+nrNqA8sxiW1PdqxrfSYObK/ftVudLVfk4I6KR+J4guJCW1ldsUq5bCmA5cIEpUwGY7s52rLeZGDtYcfWSYF3V4/ZpetX7U7/jgNCD6dVelRoaKzhGJxByX3Cj4Et9lLLF/JkjZyFuImAsxwWvL43CrtJP21d7k6ne5YZ7ExO7d3u/yGi/pmzPLJLAXOH4A2sGbSuAWB4WLc2yq5t1G1jvWrGgBevXXV5uj/pOXBaLjc1iQXK/8PIWVBOlQfsGvzTYsWAcoUc3DW2CHC8fvO6rdSX7Uh9zYqNCnE7NXnTzdYWc8e2wpjHm0dc3AsAb7RuAYBgGORXYZZ4saPMR4vrTsGyvHiCvKVJGq2CGVJOe7hnswCcmXrWvjz0Lv7sVnb78mBWLhfDTD4cHJR5rgBFAyqRW2SLfJTO/BFs7nJn27ZvAFq5sU0yfVtbW7eNtSM2IiZ7o77dwNKKyZ3+LmmqMHdPFurCAkbIeGBDaOtKc5mi9QcDp8SbAOQyJEhAV2bRy4aQoLdI8cXrly3JYnKYfgbsaO22AbuxLTebdu/GPcR7ndpgZn3CqB11nYeNx6TcXt9lolkysyEG6d9Bue9DuS/uuUxSXy77MFWbRC4pOB3oxyEPCFm8SpGYRJ5CSELArNuNXLqqNUqWbQJKpKE8Vi1GRYciorPHl49YntQlDpBJyd1+iYAYGtDhUL5SKDqPkJLH0GAMicVJbQJQUZ6x7+gxCY9qEcbYgeXFqfVIjYN+H89Ys2ZYs7XqkmXxsDt7u3ZrQC0BtshyMUInEKoh1WbvzthaFyLbu45X8i9fzlrRz+E5QUq2tCRSrfXhMVfA/nmZBoVPVUY6gbLOHDkpEnu6aNgZ25uvXrAmgLJ6vGGFehaAHFpIbsgWqtaFvfWnQ6eI5fKSHSH/SyFyc+HCzqBtoxkxi1eMcNtIhRBZQKsZD8cW8Z3CJwBHQiJzyHj3bdxn9x8/gWVDu7Ozba+986a1Rh3zCbsjcIIv8t1sMrM33n7Hrly6bf3rZJGrQ1OzJVuCEgGGGl/AnIwQHq+e7jjGvC/xwgP2Q0BxW8C6vasD27s6svqXqxbmQzSYt2wxY1E8oaaP7MpbW7goLgs8NMAIvxA4pQkQy0HRcX9xdcV/nkxRSkvWGndtNbek6tBavT1rgyXX9m659DOhABI1Fl/A/Vgcr0B1MUmXf23QfWvYNlKZVfNMSr9A6XnUH9sQgbbeHNl7PyMMAd18JeOI0wxMyaShC0sRqXgPb3kXJVQ+kN39cigEtl7ftub9pCTiZPLnbct9+Rgul7UhMS9rVnMl6233KWpIbQW5XIL7kuCxKCHOomKimfjEMy7tXAW5PLtn/QsgeMbhwjidWgiNWq6SVbyGdUB+VYZlcr2UNwAjxCLVL2gUGih1bD+/8rYTSKntN+6+n5RZs63tXfvpa2/bj98+bztX5tlnbXnJKU7em9B/SCMyDOSDCMWrUSzv+SXqkBy/3/lQCYdCYPfHuza40LHc2oblv3LCxW2YDa2fjCAynlUKJZsBLqO3e1b8Ork1Q5y2hqSy2MTHMzUm2ZjhERkreJSzhMd0pKqNcAIkWyI5g20UWbZitkC+J0USaj55LgLIJrxEjNaULsENkZMWvGAQgyd4Z3uvYzsXe/bOa9fs8oVdwt+z1Wbd8oClMsUUpgh9sWQ3tvH1sXkn5FU+rp9atge7xIvlDQePQwo48Q9XbASaDsYZyy0t2fa7O1aBfGiQcTpmBs8mHjR3v3tDAYSD4lZ6URGOt1Eu99c3Eltar1DPB3jP2PjN6hCXfJhz2abVb3HdNvHpO7CbIviMdLdcW7X7j91jFTxiNImsO6HYQsnJ2LOtK21756ev28X3bjlgrdWrMMHA4ClIzH8EU/mcp8YQZU6H9BuijKXbBNEu5AmjeEUxw4PiLzBg/7NGOQ8zy9t2f+qs3rs8tDt/dc0Kf2/JcvdmLW2wGDExXErpT7wuBmCwnSUAXZEO77RvdvH127Z9rWOV5ZylpND15prV6A1M6Qv4GZ/q8C6USZ7fvm3dWddKkCNZW/XAm++9Q8pbsjzroM6x2xfbdulnt+32hT0n2HHSsDjBiHyfQnzy4RwnArwogJUmwhDWWKIHOb7Jd5AuH0qsEBCLjP8uD+iPcEFK1Zw3QpCBlYiZYXHFdl/pm1f1rbpaIK6o/XE3ESAJM7kBshfIEvkBGSBnSwiqdli31bO9O10rVDN2BDDMUc5uYNl6sWwxyK2YL9Ld6UcwOrLHiFwP87QuYXL5rYtWmi3biFi99OZt0mUGzoCz4nHzCnFmARWaPIwugwshPN+iAQXX9Z4FPQAQ8PYxjo/Ve3ihMkGBcYpg2sHjUAi0yfkzWN+ExakwqRJbzWrdjj1cs+0srSzu9HJoucX3rallVmlWbMWWTlIrfbXkWBqVIt2eMgLDISBLk+HUXvvJu3bj8rbd/8Vjtn60gfYIFyo6KYp9AGpjPGrm25D503bORr8o2KULN5xF60tlm95CzM7EMkdzlgyZrw2H2ICFkh7FFybDiU3yHh2pkY3fHVj5aEnwQBlOnwCv65EtaqW8rdbBMBRx8DikgNU61kHwnc4AkpGCzvBrrl9dKaNV3958a9cKMZYv+db/q57ljqGAfmw+FZ/qew+PoPlJ/y90FaEyhxyUnp5dvnbN3r9y2daWobvH16yxBm0FXEekQI901b0d2bWfU2dAxdVQaa7UHLOTCw+v983fYF4UN+sShuR7O0IUReT1y6LhiWXvZf4E8D1xFI8Q2VIzJsWgsRXyWF4YgFLGsMiDxyEFHFmuAiQiQrFdurXHQMQ4g3cIjRnxGL0+srV7G7Zyb95ubnVt/D7AKFC9m3ofIRUWswAGiQBiiCqL5aK0xB3lFX7cuLVlFy5dsWPHj1p1rQAJmlrrUmQ9rKwFFopih6CLCBtyOtTGawW8UoCuEQ9QUUS73OIrFFobEK4bZKpx3UJwyEjNqj6HKEjKlKdNYIFd+pJDwu/g4RQgp9C4mkz0tEJlpcVO0V5KXA6iCZqbgu4FwtC3Inm92SjaoDjF7WYO3EaktNnOzPIeGLJCo5vr6jAln+xwq9MDQ+aF0kpz2Xpkigmu3GHxu1foF9LXC3HhLCieIbZjwlBEZga91f6BO/DIGUwu7rImVq1Gq4DUbxCStMwNouPXWEvasQzltANphApRmuSQAlSXgIWHDqcACb845DY6hKjdfuQmKhbIAKiooB0b0EZxFBN7eU4KtdBGMLTRiGzA9cP/1bXkMfBjrQIrLBHetMNfxroP0L46ATWFpor4iHKLsGTyWNVnPGdufQ8+qJfASxYXKfRA8RjlioNAAC1kLzHdIrj6+BbnAVkpV6enGI9suAcpK4lDhM71db/LPmKZBwXdF/iQB2gwuVgGwZbor9UBkQnW2G3tWbF5l2WaK+bNWsRsSjWmjn1C1shZFgAbvA6AkZL9JQRIJq5S677fc7k5SAKb3ub6VXI13iOuL7xQxZdSyyt1zUYge7FouVLWZYXEdXsBOREb5qMFjrKENWrIMV8LpTJEHrSXlbXwgFdjdcm5fYSrS14KOyswZwaFf9zhFLD4Yo/mhY+FJFgWJawtQWHI99s7ABEpLkOtPoFlzQY3WPg8vUxRkBYFGbC0h0WIV2WGBAzo/HwXBuZZfa1mwwIMEmyZWxpCwjUkdOIzIFej0DfpEp0kTlh20iVN3oaxnKDjRNu9qH4jAqR4ivBB60vpuotBap0SvgTQlfHULG7e6g2d0kb0J6UsgaA8e4xSoo8DwYVnjKnTO9TSA5iZAgHlMXDWcusr1gEH9ihoIrxkQvUmKutX6fgwyR6uL2zIl9AnHZnoPH39a+z4rN7HNQP6fZEVYZfj94nnuxgU2j65xRxXoahfqRLTCAaCR132EKDWvQvcz8JnTXL8DKtSJQrMxFPG0GqtU2laOV1ELAf4Ks2JIElApXEdAnAdPdYrXNEadd/B45AH6AIpQ4Lrx+0WSI/WGoRDnuov17sKqYW3Q2bGgIq0fu+RplPCpVsKDXIvC4mw2hjwC6ncQiq0tHvVhZbdBj+wrHecMGOLPM4T+xIG78ltrNvwDnmc7fGl9TotuCJ9wIH14AbeEbq+CNAn3zvvxOPWl2psk4W20x06eSS0Yl15X+uXHFKiwE8KEHDLQwTqB49DZ3iWuzkLwShCAEakka3pvAXmWlJcIG8PiPsppEOkSQOvQDCWqkW70+67ybRHEGLVqN+GrKgAAeG58a67a7bbpuK7NLHKEr2AWmDXobrRqGDFasV8OETjmPqPdJYxhjwrV57vJLEyxklslcZMswJWIIjiXAKze0j3CJ5AKMhT5i88QAKhijwMsMya8xhMCjl4HEIGnYhqClR0gxzBaZaBBHodGJVAUYe4u47bUN5WdzRPjShBk5ahvgUW2Ll5gdbUVV3txhJgNrBsnMXlERCnglVSwcEUgkmXinFKvML2GGOA8vkWz+Mn57KqwrFeLjhlSvg+YdnHuhN+12oU58r9WrMMJlrfrLK95vBCpIiOEyF78DjkAYsvNJlUoYE6WEGu42KS+JF2nfDu3WhFk/ogHnFaddrV98s10h9exGYNygHcWJ3i0WUO3rXnr1hUtmksV9zCJ2yIpFgKGSArcwLjUel5PDwx4jtVeLp+gCH0UoUql9c8MpbKZeV7zVNivRVesryYn1+owylIm2zFieEePA55gLMt32vBElLurZcmVBzJxRSLsrIm1LHgDVJWD5DqUHjIOnLJE+sNMGL5A5fs850W2aA5qYXrcCjO+5C0p7E0t961xeWXmhZUSGvYV5lOniSrD7CiwkFWbxJ+FRTgQoR16cMynqLUJ7YY0TrOUGZ7NHNmnE+V6w8chzxAMklIxZUGD0iJTdx6hbiTAvb6bFUT52pdycXlFX1tg+F2cntZXy7YHszTqZQXMgYfuzQmHIiwroS/C64vBD9/5Y4be4KlEnKbFBzhUeLwrJmmCkVQjlRJuHyQ21nfkHmVGgsIK6XK0rK+1i1jJFRu6hYneBDbyzRmhs6QYzjJweOQAsZjLIeqFSsjND1DoxJCsaTY0+vqnT3q+K51UYJiUovQAgRYRYLt7vWaU6IWpIUsUpEsWxLP5749AFTuXEYRC6/SOAtQkxAVskkw68D0iG8Mk6g24BCIKdUpLAWuQ7xOuMSwjr26/oQUoItpo6WU2hN6FQPK+yzld4M1XkGGxXFIAV2IkABHudalE66SRRTnsrBSnDxCuT8DQWojSJXneJQO5+7ruYygRcjqAlSFi4SXIiakvMXRGUTOTXW/ji08S4tR7Cq/51HOAE+IJByeNhffXYoHzdmg5hRLLNArqLM2YUKLtCh8kVHyGM9TqKBEkboG2WMO3jfnA/HzkALErCR4l1gV2WjWqLlxYS1kyqAFWuQOZBj4nqNNXHXmsoDi0VlS1iH+lUYVDgPCRhNKAC1WgKrGhkBK48U0LudANX9Yqgnf0CKd4qDHEWvQq0TXWYeEUo2iLCUFy5tWMIgLR3ku80nbXbKVFL/MZ8KYVXahRJYUskrtB49DCmBmZy2HtlwoDQpMRIZ0TMnTApx5vEPomJwE5bSu2PcI1FZn5kJBeVqhpEVrofMsQrrj9yLe5KznPEUQZy7UpDgtXC4uQwgvdF2ExymDCIfkMVqfPKiEMhV2MopCdsR9GktzqBehNfkF7VwHjrgpk3wsFZZw7uBuLVqT1LGGrCoXUxaQQJpM7wKxq1ttN7CUJHC7zvl6s0Y7O2u7uKHCRJbS9RJCeCBFKKT0EtHR2DrmXkJtgYASVFbWtfqc5xP23TqhX8lTZoyvgydTqCuyzDPHGo2leeR5mnNxSAZlDilURvjo8eGVfFMD5DQh17oOSg+XHXLz4pAra4ECIlHhO3sD2yCPB+zTz9hVyiGU0L1KPCoW97CSAHSZVKVzWU24sc196026xFIOC1bs6nAojxHUA1SzQ94jr5Qy1qplHp4qOwuqP5Fla5xcy/nQCS0B596m8JhzlWUUJqGVntGDA8oPUWgu1SEFqBYQjSVWPTEsgZk8QjcrDLSQhZseRVCBoh6L8xCkcuQE29R68nPG5zz8gGfIC+TSakaq26SQubHTpdvUdplAHtXHgsoYc3BS3JPOmFdzKQNpHClPbuwQXtdq+4yHMTw1TJhPWUBexTIdfiC/U67iX/fMD2aQIE6l+x/xdsgnsD7/BEGWDCg8ZC0NIJdXPF262bY2i5GmcwwuZNVkA7KHevRTnupq9bE057pPISDh2uwsSYFKmytUkEtsWEowuaziV1aSMiSBrtN3amCuwCgFcDrX2m9Bu52XcKGH5dlFnSsMwKztZyMVTBpXonYxorxuX/BEniMgPngc8gC+IKPRoEzHbNX5EybLoBDvi8eW3SI0oBaoSVoZUiCAKO9Vjg1ntx1t5eFQ0J3eGxso8xRIico9ChlXyrJYeVGVXZqU9teA71ivI0yyumhtEyVJ6Qo1YYdSr5uX8FNM89yP8qr7PSJUxCFUsQqrpHiNo66VDAg+pXSlpoPROCMwxVugVx8eC1rkfOPLJ1aHCHQfDzEdww2CjeWqV8rnZnSGPPppnjSoQyjrCJCk18GiUu388hToDAHnGlfjUtvdc+HlDTxRZ5Ns09LqhoV0g2llEgLzMlaCafG6VyEjXBDllYdpLnmmwFmxrkJJjRoprouFFR4VMovATkAqUGaNPHwdxFwXwCoDjOCxnldzgf/sTm90UavmpYbT/GAs79FLdy586/eO/ud3b0/fIgWtH19r3E2r3G/3Rx4WjPUkRj+aekJetZmF1ppc2DFWh5ZBJISspYVqBglyk7jXWY187lGYpOVlR00jHrAQVxfwSgG6x3mNLKhQ5CWFKMaldHmnPEnzMpULRSlGAgsDBHaEVEKoJVBkH0LlLkT4l4jqf1O60frXb/RG73GrE16SfxACfJJubpr/zU33lx1/yncv/v1jK4+MJtOnEep36N0FFXqEEW1aQMmns+tPJ3rwQFWfanH2/eWyvIS+EkZuKFdWb1FCemSLuL/DZjJkhK2taTxPc8jgwsShPhNLifzAlecMUJScx4Xnn6MQhZCePJVSRKq0RUd+TzhPoMihPuPRTSnoFUjxM96F2z98A31JYA7h3r7rzjXhPl38OHNm/7n/F+YPFJ4+fTo8kW3zoGT6r7D21/T4SZuNx2yxOk2iTrDT6vg8pgqw5dnQpIFCw+Mf/8Yxh8LvXd9xi5R1ZbEKLqwHKDs9tsVZjkpa1wVGaWJonLqFuyapmCegKS+QYgSUyu86F6YoO8mO/E/wCD3UQh9qDvGg/1/yUOd/PPv65Rf5yLE4npF0f2fA+aFMqHs+9jhzRj1bnsffV4TOS92Tj3e7e0/FXv5U6dgDlKyg6rVfTPqdVlivllk+2+XE4G+y8yOr/+TdGyyelhr8Qq6qha+w+yTufplUqHZWHYBTJlCBJAAU4qv3JySXF4kN6ikVWV1W7VKRCuzY9koQckZdkJVydA/e/2qQBs/+9OLN5xHKCerk4K8M+H9I8IXQf6sCFhcoLM6ePc3r7P4OhQW//9X7H8/e9aUnM/nig6X+dXrxt0B4nyd3ZyH7BQBn1glz8WbLCVEvszmKAIpdcQcpQYCqbtJNXj2ySgfBRJhUGyz4w5yE4fIILuVC5/GcJOWJ7BmUPCNMULQATy+DS8+de+/mDxaCnz5t4emzlmwecPeFTAfff6kCFhdLETf/B38EdW7/r7/OnMlSOT5aHFx/MpdGD4mcSCgKpBlWCwgZTwgtwcX4tFF5bKXusoJDdKwtvPjJL26giL4rb5W+ZHm9hPzCAnkBKUx/VZJWas0448VhIVBDxIHmS6nvPdeOSy+eP3/epaiTJy1z7pyz9gdxvpDh495/ZQUsblYsNU6e9D9QxAOU2eu/9Ujgpd8GIx4k3G2PHiFtsrjVjUifgT9vT4X2hQ09bJng+uzg4iWqPs9f4SFJXFpVoCw/ovKUl6joUo+B75J4EqW52mpQbG7w0MMeDz/svszOyrO7aelHHxFcQn+sqy/W/9H3X1sBiwEEltvb294iNJ44eTIT1cff4PH1pxHklFKhUhZWnOXpsvDuK74dEGJZKUrKuODCJHUCi6XJ7e85wgMSmSDZ7Y0S0mgYgzVWWFJ77BU/GjwT/fXrPzy3j+py9ZUVS1944dcTfCHHJ1bAYoCPZg2dj6++fobweBr0/5qEVn8QlBcbC3hywxfYaeIx6H7lNg8y7ud5MTcUkxxbrcuKYqEuDPrD6C8TP/Od+37ra2DyPDvNQRpw+4SCL9b/qRWwGOjjFNF979zjtMGeqlfyp4TUwgh2aCYZHvMmNDw1Pm8S/0Ncn3Z1ymdTsgdNc22sOMx9lfG/u/bmlR8sUNzNg9SL88X8n/T9M1PAYgEfzRqb8IiL4e5jZOxvIeBDpAoQf6x0N2UfMrzD053UFgLOzAbFlTym3Y9egtf8Sbhy4vlFiJ1mHH5XjP9K4LZYzy97/8wVsJhwE8Z18+SBrPHAA9nykeDRQTR7ku7NQ6LT2rkVERLRUexXS4WXasXcc+913wHVebaC4yTYcu7cOYXEZyq4xtbxuSlgPjxUCkxoXOSPpxfpE0VMKtNH2E36NmnwwQK0FTB8mQeZnp2Vlg+g+snMPfecSz5tjC/W8f/9XYqQGy8WonT60G8e++OHvnT8jxf4oe90zQLgFtd+nu//F0Jg44UPqyagAAAAAElFTkSuQmCC"
        with open(configPath, 'w') as configFile:
            json.dump(config, configFile, ensure_ascii=False, sort_keys=True, indent=4, separators=(',', ': '))
        json_data = json.dumps(config)
    
    #Prepare the response
    data = "\x00" + str(pack_varint(len(json_data))) + json_data.encode("utf8")
    response = pack_varint(len(data)) + data
    
    #Bind to port
    sock.bind(('', int(args['port'])))
    sock.listen(5)
    
    print "Listening on port " + str(args['port'])
    
    while 1:
        #Wait for connection
        client, address = sock.accept()
        try:
            #Packet length
            unpack_varint(client)
            
            #Packet ID
            if (unpack_varint(client) is not 0):
                continue
            
            #Protocol Version
            unpack_varint(client)
            
            #Server Address (hostname or IP) Length
            length = unpack_varint(client)
            
            #Server Address (hostname or IP)
            client.recv(length)
            
            #Server Port
            struct.unpack('>H', client.recv(1) + client.recv(1))[0]
            
            #Next State
            if (unpack_varint(client) is not 1):
                continue
            
            print "Responding to connection from " + str(address[0]) + ":" + str(address[1])
            
            #Send Response
            client.send(response)
        except:
            #Prevent crashing on old/malformed packets
            pass
        finally:
            #Close the connection
            client.close()